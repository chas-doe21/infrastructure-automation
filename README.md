# What is infrastructure automation

As we've seen before when writing code, we want to automate as much as possible: testing, quality assurance, delivery or even deployment. For that we use CI/CD pipelines and tools.

This helps us avoid repetitive steps from being run manually, and gives us more time to focus on coding.

But what about the deployment and management of our infrastructure?


## A practical example

Let's say we pushed our code to `master`, and the CD took care of deploying our new version to production, after making sure all tests are passing.

But what if we don't even have a host to deploy our code to yet?

Let's look at how it used to look like before.


## The manual approach

This approach is still very much used, even though infra automation is getting more and more popular.

Usually if you need a server, either physical or VM, you open a ticket with the infra team. They'll make sure to create the VM (or mount the physical server in a rack and connect it), install the OS on it, create the needed users, harden the system (ssh rules, firewalls, &#x2026;), make sure all your dependencies are in place, and finally deliver your server and close the ticket.

This will take some time, depending on the details and the amount of work the infra team has to take care of.


## Not only that

On top of that, usually infra takes care even of keeping all servers up-to-date with the OS updates, keep the users relevant by adding needed users and removing unneeded ones, etc.

This quickly adds up to a **lot** of work. A lot of **repetitive**, **error-prone**, **manual** work.


## There's gotta be a better way!

Infrastructure automation is all about provisioning, configuring, migrating and deploy your infrastructure in an automated manner.

This may be also referred to as `infrastructure as code` or `IaC` for short.

We automate our infrastructure with the help of a one or more tools.

It's called IaC because our infrastructure becomes a description of it, that can be committed to version control, and that is automatically provisioned and managed.


# Advantages of IaC

-   costs less than human-hours
-   it is faster
-   it is reproducible
-   it is less error-prone
-   it is less complex
-   it is more efficient


# Examples

Imagine you could take a text file, and write in it a description of all your infrastructure: all the VMs, networking, all that needs to be taken care of. Imagine now you have a tool that reads this file and makes sure your infrastructure actually corresponds to it, and if it doesn't it will make the needed adjustments.


## You don't need to imagine

Here's a list of tools that do exactly that:

-   Chef
-   Otter
-   Puppet
-   SaltStack
-   Terraform
-   Ansible / Ansible Tower


# Approaches to IaC

There are 2 approaches to IaC:

-   declarative: the IaC describes the state we wish for. If anything is not in the state described steps will be taken to bring the infrastructure to the desired state (what)
-   imperative: the IaC is a list of steps to be taken. The described steps will be taken, irrelevant of what the state is (how)


# Examples #2

Let's look at a couple of practical examples.


## Terraform

Terraform is developed by the same company that makes Vagrant, and it basically is Vagrant's bigger brother. Where Vagrant defines local, virtual infrastructure (VMs), Terraform can talk to cloud providers (AWS, Azure, GCP, &#x2026;) and other providers and can be used for our real infrastructure.

Terraform is declarative.


## Ansible

Ansible is developed by RedHat, and it is an imperative configuration management tool. It does not allow us to create or provision our infrastructure, but it allows us to configure it from start to finish in an automated manner.

It uses ssh to perform all the needed actions, and uses yaml for its files.

Ansible is imperative.


# LAB

We'll install ansible, then create a local VM and let ansible take care of configuring it.

We'll start by issuing ansible commands on the CLI, and later will write an ansible playbook to avoid having to re-type every command when needed.

OBS! Ansible can manage Windows machines, but cannot run from Windows machines. If you run Windows, make sure to deploy 2 VM: one to run ansible on, and one that will be our target. Make sure the VMs can reach each other.